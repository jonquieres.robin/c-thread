#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

//Creation du client.

int clientSocket;
struct addrinfo hints, *res;
int status;
char buffer[1024];

int main(int argc, char *argv[]) {
	//verification d'entrée du bon nombre d'arguments entrés
	if(argc != 3){
		printf("Tous les paramètres ne sont pas entrés \n");
		exit(1);
	}

	//Initialisation du socket
	memset(&hints, 0, sizeof(struct addrinfo));
		hints.ai_family = AF_UNSPEC; // IPv4 ou IPv6
		hints.ai_socktype = SOCK_STREAM;
	if ((status= getaddrinfo(argv[1], argv[2], &hints, &res)) != 0) {
			printf("Error getaddrinfo: %s\n", gai_strerror(status));
			exit(1);
	}
	if ((clientSocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1) {
			perror("Erreur ouverture du socket\n");
			exit(1);
		}

	//primitive connect :
		if(connect(clientSocket, res->ai_addr, res->ai_addrlen) == -1) {
			close(clientSocket);
			perror("Erreur de connection");
			exit(1);
		}
		freeaddrinfo(res);
		printf("Connexion avec le serveur reussie.\n");
		while(1){

			//lecture du clavier
			fgets(buffer, sizeof(buffer), stdin);

			//ecriture du client
			if(write(clientSocket, buffer, strlen(buffer)) == -1) {
				perror("erreur d'écriture\n");
				exit(1);
			}

			//lecture de l'echo du serveur
			if(read(clientSocket, buffer, sizeof(buffer)) == -1) {
				perror("Erreur de lecture\n");
				exit(1);
			}
			printf("Message reçu du serveur : %s\n", buffer);

		}
	close(clientSocket);
	exit(0);
}


