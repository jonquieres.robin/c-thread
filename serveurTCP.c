#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>

//Creation du serveur.

int serverSocket, clientSocket;
struct addrinfo hints, *res;
int status;
char buffer[1024];
size_t message;

void serveurEcho(int* socketclient){

	//boucle qui laisse le serveur en écoute
		while (1){

		//Lire message du client
			if ((message = read(clientSocket, buffer, sizeof(buffer))) == -1) {
				perror("erreur de lecture \n");
				exit(1);
			}

			printf("message reçu : %s\n", buffer);

			//write : renvoie au client un écho du message
			if(write(clientSocket, buffer, message) == -1) {
				perror("erreur d'écriture \n");
				exit(1);
			}

			// Vidage du buffer
			memset(buffer,0,sizeof(buffer));
}
}


int main(int argc, char *argv[]) {
	pthread_t thread;
	int socketclient;

	//verification d'entrée du bon nombre d'arguments entrés (./serveur + N° de port)
	if(argc != 2 ){
			fprintf(stderr, "tous les paramètres ne sont pas entrés \n");
			exit(1);
		}

	//Initialisation du socket
	memset(&hints, 0, sizeof(struct addrinfo));
		hints.ai_family = AF_UNSPEC; // IPv4 ou IPv6
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_flags = AI_PASSIVE;
	if ((status= getaddrinfo(NULL, argv[1], &hints, &res)) != 0) {
			printf("Error getaddrinfo: %s\n", gai_strerror(status));
			exit(1);
	}

	if ((serverSocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1) {
			perror("Erreur ouverture du socket\n");
			exit(1);
		}

//primitive bind :
	if(bind(serverSocket, res->ai_addr, res->ai_addrlen) != 0) {
		close(serverSocket);
		perror("Erreur du bind\n");
		exit(1);
	}

//Primitive listen :
	if(listen(serverSocket, 5) != 0){
		perror("erreur mise en place d'une file d attente");
		exit(1);
	}

	printf("Creation du serveur reussie.\n");

//primitive accept (accepte la connexion avec le client)
	struct sockaddr_in addrclient;
	socklen_t addr_size = sizeof addrclient;

	while(1){

	if((clientSocket = accept(serverSocket, (struct sockaddr *)&addrclient, &addr_size)) == -1) {
		perror("erreur accept\n");
		exit(1);
	}

	//affiche l’adresse IP et le port de l’hôte client
	printf("Adresse IP client : %s Port: %d \n", inet_ntoa(addrclient.sin_addr), ntohs(addrclient.sin_port));
	freeaddrinfo(res);

		// Ferme la connexion avec le client.
		if (message == 0) {
			close(clientSocket);
			exit(0);
		}

	if (pthread_create(&thread, NULL, serveurEcho, &socketclient) != 0) {
		perror("erreur de thread\n");
		exit(1);
		}
}
	//Arrete le serveur.
	 close(serverSocket);
	 exit(0);
}
